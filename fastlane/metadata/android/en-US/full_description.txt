Sorts all photos, located in the device's camera folder, into a date specific subfolder structure. This makes it easier to transfer specific photos to a computer, e.g. via USB cable (MTP protocol).

Either keep photos inside the camera folder tree or move or copy them to a separate directory. This can also used as a simple backup method.

Up to three levels are supported: year, month and day. The sorting can be reverted, i.e. all photos can be moved back to the base directory (but not back to the camera directory in case a destination directory is specified).

On the demand a file name prefix like "IMG_" or "PXL_" can be appended instead or completely removed. Note that this operation is not revertible.

The program does not read metadata (EXIF) from the photo files, instead it uses the data information encoded in the filenames.

To meet Google's restrictive policy, the Play Store variant of the program must use Google's proprietary Storage Access Framework when running on Android 11 or newer. As a consequence, file operations are extremely slow. This restriction partially exists also for older Android versions and not at all for the F-Droid version of the program. In this case standard file access calls can be activated alternatively, which accelerate the file operations with a factor of approximately 90 (!).